<?php
    require('animal.php');

    $sheep = new Animal("shaun");

    echo $sheep->name; // "shaun"
    echo $sheep->legs; // 2
    echo $sheep->cold_blooded; // false

    echo "<br>";

    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"; 

    echo "<br>";

    $kodok = new Frog("buduk");
    $kodok->jump();
?>
