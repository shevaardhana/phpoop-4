<?php
    class Animal{
        public $legs = 2;
        public $cold_blooded = false;
        public $name;
        public function __construct($name)
        {
            $this->name = $name;
        }
    }
    
    class Frog extends Animal{
        public $legs = 4;
        public function jump(){
            $this->name;
            echo "hop hop";
        }
    }

    class Ape extends Animal{
        public function yell(){
            $this->name;
            echo "Auooo";
        }
    }

?>